<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model
{
    protected $guarded = [];
}
