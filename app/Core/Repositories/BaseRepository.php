<?php

namespace App\Core\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository {

    protected $model;

    public function __construct(Model $model)
    {
        //
    }
}