<?php

namespace App\Core\Repositories;

use Exception;
use App\Core\StringUtil;
use App\Core\Models\ContactRequest;
use App\Exceptions\ContactRequestException;

class ContactRequestRepository extends BaseRepository implements ContactRequestRepositoryInterface
{

    /**
     * ContactRequestRepository constructor.
     * @param ContactRequest $model
     */
    public function __construct(ContactRequest $model)
    {
        $this->model = $model;
    }

    /**
     * Create a new contact request
     *
     * @param array $request
     * @return array
     */
    public function create(array $request): array
    {
        try {
            StringUtil::splitName($request['full_name'])->each(function ($i, $k) use (&$request) {
                $request[$k] = $i;
            });

            $result = $this->model->create(
                collect($request)->except('full_name')->toArray()
            );
            return (null !== $result) ? $result->toArray() : [];
        } catch (Exception $e) {
            throw new ContactRequestException($e->getMessage());
        }
    }
}