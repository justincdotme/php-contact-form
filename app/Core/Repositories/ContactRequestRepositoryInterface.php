<?php

namespace App\Core\Repositories;

interface ContactRequestRepositoryInterface
{
    /**
     * @param array $request
     */
    public function create(array $request);
}