<?php

namespace App\Core\Repositories;

use Exception;
use App\Core\StringUtil;
use App\Core\Models\ContactRequest;
use App\Exceptions\ContactRequestException;

class FakeContactRequestRepository extends BaseRepository implements ContactRequestRepositoryInterface
{
    /**
     * ContactRequestRepository constructor.
     * @param ContactRequest $model
     */
    public function __construct(ContactRequest $model)
    {
        $this->model = $model;
    }

    /**
     * Fake create method.
     * Returns argument + full_name attribute.
     *
     * @param array $request
     * @return array
     */
    public function create(array $request): array
    {
        StringUtil::splitName($request['full_name'])->each(function ($i, $k) use (&$request) {
            $request[$k] = $i;
        });

        return $request;
    }
}