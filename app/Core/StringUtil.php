<?php

namespace App\Core;

class StringUtil
{
    /**
     * Split a name string into first and last.
     * Last name is the string following the last space.
     * First name is the string(s) preceding the last space.
     *
     * @param $fullName
     * @return \Illuminate\Support\Collection
     */
    public static function splitName($fullName)
    {
        $fullName = trim($fullName);
        $lastName = (strpos($fullName, ' ') === false) ? null : preg_replace('#.*\s([\w-]*)$#', '$1', $fullName);
        $firstName = trim( preg_replace('#'.$lastName.'#', '', $fullName ) );

        return collect([
            'first_name' => $firstName,
            'last_name' => $lastName
        ]);
    }
}