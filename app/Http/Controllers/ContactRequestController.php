<?php

namespace App\Http\Controllers;

use App\Rules\FullName;
use App\Rules\PhoneNumber;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Core\Repositories\ContactRequestRepositoryInterface;

class ContactRequestController extends Controller
{
    protected $repository;

    /**
     * ContactRequestController constructor.
     * @param ContactRequestRepositoryInterface $contactRequestRepository
     */
    public function __construct(ContactRequestRepositoryInterface $contactRequestRepository)
    {
        $this->repository = $contactRequestRepository;
    }

    /**
     * Store a new contact request
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'full_name' => ['required', new FullName],
                'email' => ['required', 'email'],
                'message' => ['required'],
                'phone' => [new PhoneNumber]
            ]
        );

        $this->repository->create(
            $request->only(['full_name', 'email', 'phone', 'message'])
        );

        return response()->json([
            'status' => 'success'
        ], 201);
    }
}
