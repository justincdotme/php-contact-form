<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Core\Models\ContactRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactRequestNotification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $contactRequest;

    /**
     * Create a new message instance.
     *
     * @param ContactRequest $contactRequest
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Contact Request')
            ->view('email.contact-request', [
                'contactRequest' => [
                    'First Name' => $this->contactRequest->first_name,
                    'Last Name' => $this->contactRequest->last_name,
                    'Email Address' => $this->contactRequest->email,
                    'Phone Number' => $this->contactRequest->phone,
                    'Message' => $this->contactRequest->message ?? ''
                ]
            ]);
    }
}
