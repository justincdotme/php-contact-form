<?php

namespace App\Observers;

use App\Core\Models\ContactRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactRequestNotification;

class ContactRequestObserver
{
    /**
     * Send an email when a contact request is created.
     *
     * @param ContactRequest $contactRequest
     * @return void
     */
    public function created(ContactRequest $contactRequest)
    {
        Mail::to(config('mail.to'))
            ->send(new ContactRequestNotification($contactRequest));
    }
}
