<?php

namespace App\Providers;

use App\Core\Models\ContactRequest;
use Illuminate\Support\ServiceProvider;
use App\Observers\ContactRequestObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        ContactRequest::observe(ContactRequestObserver::class);
    }
}
