<?php

namespace App\Rules;

use App\Core\StringUtil;
use Illuminate\Contracts\Validation\Rule;

class FullName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (
            (!StringUtil::splitName($value)->contains(null)) &&
            preg_match_all('/^[a-zA-Z]+(([\',. -][a-zA-Z ])?[a-zA-Z]*)*$/i', $value)
        );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The full name field is invalid.';
    }
}
