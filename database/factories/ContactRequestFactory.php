<?php

use Faker\Generator as Faker;

$factory->define(\App\Core\Models\ContactRequest::class, function (Faker $faker) {
    return [
        'first_name' => 'Foo',
        'last_name' => 'McBar',
        'email' => 'foo@bar.com',
        'phone' => '123-456-7890',
        'message' => "I'm very interested in working for DI and I'd like to set up a team interview."
    ];
});
