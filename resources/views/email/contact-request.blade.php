<h1>New Contact Request</h1>
<p>A new contact request has been submitted</p>
<ul>
    @foreach ($contactRequest as $key => $requestField)
        <li><strong>{{ $key }}:</strong> {{ $requestField }}</li>
    @endforeach
</ul>