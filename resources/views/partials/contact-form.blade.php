<script>
    //Make named routes available to Vue components
    window.routes = {
        'contact-form': "{{ route('contact-requests.store') }}"
    };
</script>

<div id="app">
    <contact-form></contact-form>
</div>