<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Core\Models\ContactRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactRequestNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactRequestObserverTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function creating_a_contact_request_queues_email()
    {
        Mail::fake();

        $request = factory(ContactRequest::class)->create();

        Mail::assertQueued(ContactRequestNotification::class);
    }
}
