<?php

namespace Tests\Feature\Mail;

use Tests\TestCase;
use App\Core\Models\ContactRequest;
use App\Mail\ContactRequestNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactRequestNotificationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     */
    public function email_contains_correct_content()
    {
        $contactRequest = factory(ContactRequest::class)->create();

        $rendered = $this->renderMailable(new ContactRequestNotification($contactRequest));

        $this->assertEquals('New Contact Request', $rendered['subject']);
        $this->assertContains(e($contactRequest->first_name), $rendered['email']);
        $this->assertContains(e($contactRequest->last_name), $rendered['email']);
        $this->assertContains(e($contactRequest->email), $rendered['email']);
        $this->assertContains(e($contactRequest->phone), $rendered['email']);
        $this->assertContains(e($contactRequest->message), $rendered['email']);
    }


    /**
     * Render the contents of a mailable.
     * Allows us to run assertions against it's contents.
     *
     * @param $mailable
     * @return array
     * @throws \Throwable
     */
    protected function renderMailable($mailable)
    {
        $built = $mailable->build();
        return [
            'subject' => $built->subject,
            'email' => view(
                $mailable->view,
                $mailable->buildViewData()
            )->render()
        ];
    }
}
