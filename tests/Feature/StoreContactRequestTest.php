<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Core\Models\ContactRequest;
use App\Core\Repositories\FakeContactRequestRepository;
use App\Core\Repositories\ContactRequestRepositoryInterface;

class StoreContactRequestTest extends TestCase
{

    protected $response;
    protected $formData;

    public function setUp(): void
    {
        parent::setUp();

        $this->app->bind(ContactRequestRepositoryInterface::class, function ($app) {
            return new FakeContactRequestRepository(new ContactRequest());
        });

        $this->formData = $this->createFakeFormData();
    }

    /**
     * @test
     */
    public function user_can_submit_contact_form()
    {
        $this->response = $this->json('POST', route('contact-requests.store'), $this->formData);

        $this->response->assertStatus(201);
        $this->response->assertJson([
            'status' => 'success'
        ]);
    }

    /**
     * @test
     */
    public function full_name_is_required()
    {
        unset($this->formData['full_name']);

        $this->response = $this->json('POST', route('contact-requests.store'),  $this->formData);

        $this->assertFieldHasValidationError('full_name');
    }

    /**
     * @test
     */
    public function full_name_must_be_valid()
    {
        $this->formData['full_name'] = 'Cyberdyne T-1000';

        $this->response = $this->json('POST', route('contact-requests.store'), $this->formData);

        $this->assertFieldHasValidationError('full_name');
    }

    /**
     * @test
     */
    public function email_address_is_required()
    {
        unset($this->formData['email']);

        $this->response = $this->json('POST', route('contact-requests.store'), $this->formData);

        $this->assertFieldHasValidationError('email');
    }

    /**
     * @test
     */
    public function email_address_must_be_valid()
    {
        $this->formData['email'] = 'banana';

        $this->response = $this->json('POST', route('contact-requests.store'), $this->formData);

        $this->assertFieldHasValidationError('email');
    }

    /**
     * @test
     */
    public function phone_number_is_not_required()
    {
        $this->formData['phone'] = '';

        $this->response = $this->json('POST', route('contact-requests.store'), $this->formData);

        $this->response->assertStatus(201);
    }

    /**
     * @test
     */
    public function phone_number_must_be_valid()
    {
        $this->formData['phone'] = 'banana';

        $this->response = $this->json('POST', route('contact-requests.store'), $this->formData);

        $this->assertFieldHasValidationError('phone');
    }

    /**
     * @test
     */
    public function message_is_required()
    {
        unset($this->formData['message']);

        $this->response = $this->json('POST', route('contact-requests.store'), $this->formData);

        $this->assertFieldHasValidationError('message');
    }

    /**
     * Helper method to test that a specific field has a validation error.
     *
     * @param $field
     */
    protected function assertFieldHasValidationError($field)
    {
        $jsonResponse= $this->response->decodeResponseJson();
        if (!array_key_exists('errors', $jsonResponse)) {
            $this->fail('There were no validation errors in response.');
        }
        $this->response->assertStatus(422);
        $this->assertArrayHasKey($field, $jsonResponse['errors']);
    }
}
