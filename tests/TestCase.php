<?php

namespace Tests;

use App\Core\Models\ContactRequest;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Helper method for creating fake form data.
     *
     * @param bool $fullName
     * @return array
     */
    protected function createFakeFormData($fullName=true)
    {
        $data = factory(ContactRequest::class)->make();
        if ($fullName) {
            $data = array_merge(
                $data->getAttributes(),
                ['full_name' => "{$data->first_name} {$data->last_name}"]
            );
        }
        return collect($data)->toArray();
    }
}
