<?php

namespace Tests\Unit;

use Mockery;
use TypeError;
use Tests\TestCase;
use App\Core\Models\ContactRequest;
use App\Exceptions\ContactRequestException;
use App\Core\Repositories\ContactRequestRepository;

class ContactRequestRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function create_method_splits_name_to_first_and_last()
    {
        $modelMock = Mockery::mock(ContactRequest::class);
        $contactRequest = new ContactRequestRepository($modelMock);
        $formDataWithSplitName = $this->createFakeFormData(false);
        $formDataWithFullName = $this->createFakeFormData();

        $modelMock->shouldReceive('create')
            ->with($formDataWithSplitName);

        $contactRequest->create($formDataWithFullName);
    }

    /**
     * @test
     */
    public function create_method_requires_array_as_argument()
    {
        $modelMock = Mockery::mock(ContactRequest::class);
        $contactRequest = new ContactRequestRepository($modelMock);

        $this->expectException(TypeError::class);

        $contactRequest->create(1234);
    }

    /**
     * @test
     */
    public function it_throws_contact_request_exception()
    {
        $modelMock = Mockery::mock(ContactRequest::class);
        $contactRequest = new ContactRequestRepository($modelMock);
        $formData = factory(ContactRequest::class)->make()->getAttributes();
        $modelMock->shouldReceive('foo');

        $this->expectException(ContactRequestException::class);

        $contactRequest->create($formData);
    }
}
