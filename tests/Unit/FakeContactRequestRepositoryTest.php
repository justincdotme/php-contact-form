<?php

namespace Tests\Unit;

use TypeError;
use Tests\TestCase;
use App\Core\Models\ContactRequest;
use App\Core\Repositories\FakeContactRequestRepository;

class FakeContactRequestRepositoryTest extends TestCase
{
    /**
     * @test
     */
    public function create_method_returns_argument_and_full_name()
    {
        $contactRequest = new FakeContactRequestRepository(new ContactRequest());

        $formData = $this->createFakeFormData();

        $output = $contactRequest->create($formData);

        $this->assertEquals($formData, $output);
    }

    /**
     * @test
     */
    public function create_method_requires_array_as_argument()
    {
        $contactRequest = new FakeContactRequestRepository(new ContactRequest());

        $this->expectException(TypeError::class);

        $contactRequest->create(1234);
    }
}
