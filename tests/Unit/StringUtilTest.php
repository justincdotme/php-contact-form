<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Core\StringUtil;
use Illuminate\Support\Collection;

class StringUtilTest extends TestCase
{
    /**
     * @test
     */
    public function it_properly_formats_1_string_name()
    {
        $name = "Foo";

        $result = StringUtil::splitName($name);

        $this->assertCount(2, $result);
        $this->assertNull($result->last());
        $this->assertEquals('Foo', $result->first());
        $this->assertInstanceOf(Collection::class, $result);
    }

    /**
     * @test
     */
    public function it_properly_formats_2_string_name()
    {
        $name = "Foo McBar";

        $result = StringUtil::splitName($name);

        $this->assertCount(2, $result);
        $this->assertEquals('Foo', $result->first());
        $this->assertEquals('McBar', $result->last());
        $this->assertInstanceOf(Collection::class, $result);
    }

    /**
     * @test
     */
    public function it_properly_formats_3_string_name()
    {
        $name = "Foo Bar McBaz";

        $result = StringUtil::splitName($name);

        $this->assertCount(2, $result);
        $this->assertEquals('Foo Bar', $result->first());
        $this->assertEquals('McBaz', $result->last());
        $this->assertInstanceOf(Collection::class, $result);
    }
}
