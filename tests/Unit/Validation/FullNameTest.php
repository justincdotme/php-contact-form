<?php

namespace Tests\Unit\Validation;

use Tests\TestCase;
use App\Rules\FullName;

class FullNameValidatorTest extends TestCase
{
    /**
     * @test
     */
    public function valid_names_have_at_least_2_strings()
    {
        $this->assertTrue((new FullName())->passes('full_name', 'Foo McBar'));
    }

    /**
     * @test
     */
    public function valid_names_have_can_have_hyphens()
    {
        $this->assertTrue((new FullName())->passes('full_name', 'Foo McBar-Baz'));
    }

    /**
     * @test
     */
    public function invalid_names_have_1_string()
    {
        $this->assertFalse((new FullName())->passes('full_name', 'Foo'));
    }

    /**
     * @test
     */
    public function invalid_names_have_integers()
    {
        $this->assertFalse((new FullName())->passes('full_name', 'Foo1 Bar'));
    }

    /**
     * @test
     */
    public function invalid_names_have_underscores()
    {
        $this->assertFalse((new FullName())->passes('full_name', 'Fo_o Bar'));
    }
}
