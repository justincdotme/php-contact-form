<?php

namespace Tests\Unit\Validation;

use Tests\TestCase;
use App\Rules\PhoneNumber;

class PhoneNumberValidatorTest extends TestCase
{
    /**
     * @test
     */
    public function valid_phone_numbers_pass()
    {
        $this->assertTrue((new PhoneNumber())->passes('phone_number', '888-888-8888'));
        $this->assertTrue((new PhoneNumber())->passes('phone_number', '5555555555'));
        $this->assertTrue((new PhoneNumber())->passes('phone_number', '5555555555 ext.123'));
        $this->assertTrue((new PhoneNumber())->passes('phone_number', '444 111-1111'));
        $this->assertTrue((new PhoneNumber())->passes('phone_number', '(000) 000-0000'));
        $this->assertTrue((new PhoneNumber())->passes('phone_number', '+1 555 555 5555'));
        $this->assertTrue((new PhoneNumber())->passes('phone_number', '000-000-000-1111'));
    }

    /**
     * @test
     */
    public function empty_phone_number_is_not_checked()
    {
        $this->assertTrue((new PhoneNumber())->passes('phone_number', ''));
    }

    /**
     * @test
     */
    public function invalid_phone_numbers_fail()
    {
        $this->assertFalse((new PhoneNumber())->passes('phone_number', 'banana'));
    }
}
